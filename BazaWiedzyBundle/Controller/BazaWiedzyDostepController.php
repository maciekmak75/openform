<?php

namespace BazaWiedzyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BazaWiedzyBundle\Entity\BWGrupa;
use BazaWiedzyBundle\Entity\BWGrupyCzlonkowie;

class BazaWiedzyDostepController extends Controller {

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bwGrupy", name="bwGrupy")
     */
    public function bwGrupyAction() {
        return $this->render('BazaWiedzyBundle::bwGrupy.html.twig');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getGroups" ,name = "bw_getGroups", options={"expose"=true})
     */
    public function bw_getGroupsAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $grupy = $em->getRepository('BazaWiedzyBundle:BWGrupa')->findAll();
        $dane = [];
        foreach ($grupy as $grupa) {
            $dane[] = array(
                'id' => $grupa->getId(),
                'nazwa' => $grupa->getNazwa(),
                'akcje' => '<button class="btn btn-danger" type="button" id="' . $grupa->getId() . '">Usuń</button>'
            );
        }
        return $this->json(
                        array(
                            'data' => $dane ? $dane : array(
                        'nazwa' => '',
                            )
                        )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_addGroup" ,name = "bw_addGroup", options={"expose"=true})
     */
    public function bw_addGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $grupa = new BWGrupa();
        $grupa->setNazwa($data['nazwa']);
        $em->persist($grupa);
        $em->flush();
        return new Response(
                '<html><body>Dodano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getUsersGroup" ,name = "bw_getUsersGroup", options={"expose"=true})
     */
    public function bw_getUsersGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BazaWiedzyBundle:BWGrupyCzlonkowie')->findByIdGrupa($data['idGroup']);
        $dane = [];
        foreach ($users as $user) {
            $dane[] = array(
                'imieNazwisko' => $user->getIdUzytkownik()->__toString(),
                'admin' => $user->getCzyAdmin() ? 'TAK' : 'NIE',
                'akcje' => '<button class="btn btn-danger" type="button" id="' . $user->getIdUzytkownik()->getId() . '">Usuń</button>'
            );
        }

        return $this->json(
                        array(
                            'data' => $dane ? $dane : array(
                        'imieNazwisko' => '',
                        'akcje' => ''
                            )
                        )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getNotMemberGroup" ,name = "bw_getNotMemberGroup", options={"expose"=true})
     */
    public function bw_getNotMemberGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BazaWiedzyBundle:BWGrupyCzlonkowie')->getNotMembersSelect($data['idGroup']);

        return $this->json(
                        $users
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_addMemberGroup" ,name = "bw_addMemberGroup", options={"expose"=true})
     */
    public function bw_addMemberGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $userMember = new BWGrupyCzlonkowie();
        $userMember->setIdGrupa($em->getReference('BazaWiedzyBundle:BWGrupa', $data['idGroup']));
        $userMember->setIdUzytkownik($em->getReference('OcenyBundle:Uzytkownik', $data['idUser']));
        if ($data['isAdmin'])
            $userMember->setCzyAdmin(true);
        $em->persist($userMember);
        $em->flush();

        return new Response(
                '<html><body>Dodano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_removeGroup", name = "bw_removeGroup", options={"expose"=true})
     */
    public function bw_removeGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('BazaWiedzyBundle:BWGrupa')->find($data['idGroup']);
        $members = $em->getRepository('BazaWiedzyBundle:BWGrupyCzlonkowie')->findByIdGrupa($data['idGroup']);
        if ($members) {
            foreach ($members as $member) {
                $em->remove($member);
            }
            $em->flush();
        }
        $em->remove($group);
        $em->flush();
        return new Response(
                '<html><body>Usunięto pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_removeMemberGroup", name = "bw_removeMemberGroup", options={"expose"=true})
     */
    public function bw_removeMemberGroupAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $member = $em->getRepository('BazaWiedzyBundle:BWGrupyCzlonkowie')->findOneBy(array('idUzytkownik' => $data['idUser'], 'idGrupa' => $data['idGroup']));
        $em->remove($member);
        $em->flush();
        return new Response(
                '<html><body>Usunięto pomyślnie</body></html>'
        );
    }

}
