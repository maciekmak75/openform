<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWGrupyCzlonkowie
 *
 * @ORM\Table(name="b_w_grupy_czlonkowie")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWGrupyCzlonkowieRepository")
 */
class BWGrupyCzlonkowie {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="OcenyBundle\Entity\Uzytkownik", inversedBy="id")

     */
    private $idUzytkownik;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWGrupa", inversedBy="id")

     */
    private $idGrupa;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="czyAdmin", type="boolean", nullable=true)
     */
    private $czyAdmin;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set czyAdmin.
     *
     * @param bool|null $czyAdmin
     *
     * @return BWGrupyCzlonkowie
     */
    public function setCzyAdmin($czyAdmin = null) {
        $this->czyAdmin = $czyAdmin;

        return $this;
    }

    /**
     * Get czyAdmin.
     *
     * @return bool|null
     */
    public function getCzyAdmin() {
        return $this->czyAdmin;
    }


    /**
     * Set idUzytkownik.
     *
     * @param \OcenyBundle\Entity\Uzytkownik|null $idUzytkownik
     *
     * @return BWGrupyCzlonkowie
     */
    public function setIdUzytkownik(\OcenyBundle\Entity\Uzytkownik $idUzytkownik = null)
    {
        $this->idUzytkownik = $idUzytkownik;

        return $this;
    }

    /**
     * Get idUzytkownik.
     *
     * @return \OcenyBundle\Entity\Uzytkownik|null
     */
    public function getIdUzytkownik()
    {
        return $this->idUzytkownik;
    }

    /**
     * Set idGrupa.
     *
     * @param \BazaWiedzyBundle\Entity\BWGrupa|null $idGrupa
     *
     * @return BWGrupyCzlonkowie
     */
    public function setIdGrupa(\BazaWiedzyBundle\Entity\BWGrupa $idGrupa = null)
    {
        $this->idGrupa = $idGrupa;

        return $this;
    }

    /**
     * Get idGrupa.
     *
     * @return \BazaWiedzyBundle\Entity\BWGrupa|null
     */
    public function getIdGrupa()
    {
        return $this->idGrupa;
    }
}
