<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWPliki
 *
 * @ORM\Table(name="b_w_pliki")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWPlikiRepository")
 */
class BWPliki {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=255)
     */
    private $nazwa;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWArtykul", inversedBy="id")

     */
    private $idArtykul;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nazwa.
     *
     * @param string $nazwa
     *
     * @return BWPliki
     */
    public function setNazwa($nazwa) {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa.
     *
     * @return string
     */
    public function getNazwa() {
        return $this->nazwa;
    }


    /**
     * Set idArtykul.
     *
     * @param \BazaWiedzyBundle\Entity\BWArtykul|null $idArtykul
     *
     * @return BWPliki
     */
    public function setIdArtykul(\BazaWiedzyBundle\Entity\BWArtykul $idArtykul = null)
    {
        $this->idArtykul = $idArtykul;

        return $this;
    }

    /**
     * Get idArtykul.
     *
     * @return \BazaWiedzyBundle\Entity\BWArtykul|null
     */
    public function getIdArtykul()
    {
        return $this->idArtykul;
    }
}
