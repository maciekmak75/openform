<?php

namespace BazaWiedzyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BazaWiedyController extends Controller {

    /**
     * @Route("/bazawiedzy", name="bazawiedzy")
     */
    public function bazawiedzyAction() {
        return $this->render('BazaWiedzyBundle::bazaWiedzy.html.twig');
    }

    /**
     * @Route("/bw_getTree/{idParent}" ,name = "bw_getTree", options={"expose"=true})
     */
    public function bw_getTreeAction($idParent = false) {
        $em = $this->getDoctrine()->getManager();
        if ($idParent) {
            $tree = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic($idParent);
        } else {
            $tree = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic(NULL);
        }
        if (!$tree) {
            return $this->json([]);
        }
        foreach ($tree as $row) {
            $sub_data['key'] = $row->getId();
            //$sub_data['name'] = $row->getNazwa();
            $sub_data['title'] = $row->getNazwa();
            $sub_data['parent_id'] = $row->getIdRodzic() ? $row->getIdRodzic()->getId() : false;
            $sub_data['lazy'] = true;
            $data[] = $sub_data;
        }
        foreach ($data as $key => &$value) {
            $output[$value['key']] = &$value;
        }
        foreach ($data as $key => &$value) {
            if ($value['parent_id'] && isset($output[$value['parent_id']])) {
                $output[$value['parent_id']]['children'][] = &$value;
            }
        }
        foreach ($data as $key => &$value) {
            if ($value["parent_id"] && isset($output[$value["parent_id"]])) {
                unset($data[$key]);
            }
        }
        /* echo '<pre>';
          \Doctrine\Common\Util\Debug::dump($data);
          echo '</pre>'; */
        return $this->json($data);

        //return $this->render('BazaWiedzyBundle::bazaWiedzy.html.twig');
    }

    /**
     * @Route("/bw_updateTree" ,name = "bw_updateTree", options={"expose"=true})
     */
    public function bw_updateTreeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $kategoria->setIdRodzic($em->getReference('BazaWiedzyBundle:BWKategorie', $data['destination']));
        $em->persist($kategoria);
        $em->flush();
        return new Response(
                '<html><body>Edytowano pomyślnie</body></html>'
        );
    }
    
        /**
     * @Route("/bw_updateNode" ,name = "bw_updateNode", options={"expose"=true})
     */
    public function bw_updateNodeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $kategoria->setNazwa( $data['nazwa']);
        $em->persist($kategoria);
        $em->flush();
        return new Response(
                '<html><body>Edytowano pomyślnie</body></html>'
        );
    }

}
