<?php

namespace BazaWiedzyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BazaWiedzyBundle\Entity\BWKategorie;
use BazaWiedzyBundle\Entity\BWHistoriaZmian;
use BazaWiedzyBundle\Entity\BWPliki;
use BazaWiedzyBundle\Entity\BWArtykul;
use Symfony\Component\Filesystem\Filesystem;

class BazaWiedzyController extends Controller {

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bazawiedzy", name="bazawiedzy")
     */
    public function bazawiedzyAction() {
        return $this->render('BazaWiedzyBundle::bazaWiedzy.html.twig');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getTree/{idParent}" ,name = "bw_getTree", options={"expose"=true})
     */
    public function bw_getTreeAction($idParent = false) {
        $em = $this->getDoctrine()->getManager();
        if ($idParent) {
            $tree = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic($idParent);
        } else {
            $tree = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic(NULL);
        }
        if (!$tree) {
            return $this->json([]);
        }
        foreach ($tree as $row) {
            $sub_data['key'] = $row->getId();
            $sub_data['folder'] = true; // $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic($row->getId()) ? true : false;
            $sub_data['title'] = $row->getNazwa();
            $sub_data['parent_id'] = $row->getIdRodzic() ? $row->getIdRodzic()->getId() : false;
            $sub_data['lazy'] = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic($row->getId()) ? true : false;
            $data[] = $sub_data;
        }
        foreach ($data as $key => &$value) {
            $output[$value['key']] = &$value;
        }
        foreach ($data as $key => &$value) {
            if ($value['parent_id'] && isset($output[$value['parent_id']])) {
                $output[$value['parent_id']]['children'][] = &$value;
            }
        }
        foreach ($data as $key => &$value) {
            if ($value["parent_id"] && isset($output[$value["parent_id"]])) {
                unset($data[$key]);
            }
        }
        /* echo '<pre>';
          \Doctrine\Common\Util\Debug::dump($data);
          echo '</pre>'; */
        return $this->json($data);

        //return $this->render('BazaWiedzyBundle::bazaWiedzy.html.twig');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_updateTree" ,name = "bw_updateTree", options={"expose"=true})
     */
    public function bw_updateTreeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $kategoria->setIdRodzic($em->getReference('BazaWiedzyBundle:BWKategorie', $data['destination']));
        $em->persist($kategoria);
        $em->flush();
        return new Response(
                '<html><body>Edytowano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_updateNode" ,name = "bw_updateNode", options={"expose"=true})
     */
    public function bw_updateNodeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $kategoria->setNazwa($data['nazwa']);
        $em->persist($kategoria);
        $em->flush();
        return new Response(
                '<html><body>Edytowano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_addNode" ,name = "bw_addNode", options={"expose"=true})
     */
    public function bw_addNodeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $kategoria = new BWKategorie();
        //$kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $kategoria->setIdRodzic($em->getReference('BazaWiedzyBundle:BWKategorie', $data['node']));
        $kategoria->setNazwa($data['nazwa']);
        $em->persist($kategoria);
        $em->flush();
        return $this->json(array('id' => $kategoria->getId()));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_removeNode" ,name = "bw_removeNode", options={"expose"=true})
     */
    public function bw_removeNodeAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->getRepository('BazaWiedzyBundle:BWKategorie')->removeNode($data['node']);
        return new Response(
                '<html><body>Usunięto pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getArticles" ,name = "bw_getArticles", options={"expose"=true})
     */
    public function bw_getArticlesAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $artykuly = $em->getRepository('BazaWiedzyBundle:BWArtykul')->findByIdKategorie($data['node']);
        $dane = [];
        foreach ($artykuly as $artykul) {
            $logs = $em->getRepository('BazaWiedzyBundle:BWHistoriaZmian')->findOneBy(array('idArtykul' => $artykul->getId()), array('data' => 'DESC'));
            $dane[] = array(
                'id' => $artykul->getId(),
                'tytul' => $artykul->getTytul(),
                'dataAktualizacji' => $logs->getData()->format('d-m-Y'),
                'ktoAktualizowal' => $logs->getIdUzytkownik()->__toString(),
            );
        }

        return $this->json(
                        array(
                            'data' => $dane ? $dane : array(
                        'tytul' => '',
                        'dataAktualizacji' => '',
                        'ktoAktualizowal' => ''
                            )
                        )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getArticlesLogs" ,name = "bw_getArticlesLogs", options={"expose"=true})
     */
    public function bw_getArticlesLogsAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        //$artykul = $em->getRepository('BazaWiedzyBundle:BWArtykul')->find($data['idArticle']);
        $logs = $em->getRepository('BazaWiedzyBundle:BWHistoriaZmian')->findByIdArtykul($data['idArticle']);
        $dane = [];
        foreach ($logs as $log) {
            //$logs = $em->getRepository('BazaWiedzyBundle:BWHistoriaZmian')->findOneBy(array('idArtykul' => $artykul->getId()), array('data' => 'DESC'));
            $dane[] = array(
                'dataAktualizacji' => $log->getData()->format('d-m-Y H:i:s'),
                'ktoAktualizowal' => $log->getIdUzytkownik()->__toString()
            );
        }

        return $this->json(
                        array(
                            'data' => $dane ? $dane : array(
                        'dataAktualizacji' => '',
                        'ktoAktualizowal' => ''
                            )
                        )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getArticleFiles" ,name = "bw_getArticleFiles", options={"expose"=true})
     */
    public function bw_getArticleFilesAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository('BazaWiedzyBundle:BWPliki')->findByIdArtykul($data['idArticle']);
        $dane = [];
        foreach ($logs as $log) {
            //$logs = $em->getRepository('BazaWiedzyBundle:BWHistoriaZmian')->findOneBy(array('idArtykul' => $artykul->getId()), array('data' => 'DESC'));
            $dane[] = array(
                'tytul' => $log->getNazwa(),
                'akcje' => '<button class="btn btn-danger" type="button" id="' . $log->getId() . '">Usuń</button>'
            );
        }
        return $this->json(
                        array(
                            'data' => $dane ? $dane : array(
                        'tytul' => '',
                            )
                        )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getArticleContent" ,name = "bw_getArticleContent", options={"expose"=true})
     */
    public function bw_getArticleContentAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $artykul = $em->getRepository('BazaWiedzyBundle:BWArtykul')->find($data['idArticle']);

        return $this->json(
                        $artykul
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_updateArticleContent" ,name = "bw_updateArticleContent", options={"expose"=true})
     */
    public function bw_updateArticleContentAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $artykul = $em->getRepository('BazaWiedzyBundle:BWArtykul')->find($data['idArticle']);
        $artykul->setOpis($data['opis']);
        $artykul->setTytul($data['tytul']);
        $this->createBWLog($data['idArticle']);
        $em->persist($artykul);
        $em->flush();
        //$this->addFlash('success', 'Edytowano pomyslnie');
        return new Response(
                '<html><body>Edytowano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_addArticle" ,name = "bw_addArticle", options={"expose"=true})
     */
    public function bw_addArticleAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $artykul = new BWArtykul();
        //$kategoria = $em->getRepository('BazaWiedzyBundle:BWKategorie')->find($data['node']);
        $artykul->setIdKategorie($em->getReference('BazaWiedzyBundle:BWKategorie', $data['node']));
        $artykul->setTytul($data['tytul']);
        $em->persist($artykul);
        $em->flush();
        $this->createBWLog($artykul->getId());
        $em->flush();
        return new Response(
                '<html><body>Dodano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_uploadFile", name = "bw_uploadFile", options={"expose"=true})
     */
    public function bw_uploadFileAction(Request $request) {
        $output = array('uploaded' => false);
        // get the file from the request object
        $file = $request->files->get('file');
        $data = $request->request->all();
        // generate a new filename (safer, better approach)
        // To use original filename, $fileName = $this->file->getClientOriginalName();
        $em = $this->getDoctrine()->getManager();
        $fileName = $file->getClientOriginalName();

        // set your uploads directory
        $uploadDir = $this->get('kernel')->getRootDir() . '/../web/bw/' . $data['idArtykul'] . '/';
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }
        if ($file->move($uploadDir, $fileName)) {
            $output['uploaded'] = true;
            $output['fileName'] = $fileName;
        }

        if (!$em->getRepository('BazaWiedzyBundle:BWPliki')->findBy(array('idArtykul' => $data['idArtykul'], 'nazwa' => $fileName))) {
            $plik = new BWPliki();
            $plik->setIdArtykul($em->getReference('BazaWiedzyBundle:BWArtykul', $data['idArtykul']));
            $plik->setNazwa($fileName);
            $em->persist($plik);
        }
        $this->createBWLog($data['idArtykul']);
        $em->flush();
        return new Response(
                '<html><body>Dodano pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_deleteFile", name = "bw_deleteFile", options={"expose"=true})
     */
    public function bw_deleteFileAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('BazaWiedzyBundle:BWPliki')->find($data['idFile']);
        $idArtykul = $file->getIdArtykul()->getId();
        $path = $this->get('kernel')->getRootDir() . '/../web/bw/' . $idArtykul . '/' . $file->getNazwa();

        $fs = new Filesystem();
        $fs->remove($path);
        $em->remove($file);
        $this->createBWLog($idArtykul);
        $em->flush();
        return new Response(
                '<html><body>Usunięto pomyślnie</body></html>'
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_deleteArticle", name = "bw_deleteArticle", options={"expose"=true})
     */
    public function bw_deleteArticleAction(Request $request) {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $files = $em->getRepository('BazaWiedzyBundle:BWPliki')->findByIdArtykul($data['idArticle']);
        foreach ($files as $file) {

            $path = $this->get('kernel')->getRootDir() . '/../web/bw/' . $data['idArticle'] . '/' . $file->getNazwa();

            $fs = new Filesystem();
            $fs->remove($path);
            $em->remove($file);
        }
        $logs = $em->getRepository('BazaWiedzyBundle:BWHistoriaZmian')->findByIdArtykul($data['idArticle']);
        foreach ($logs as $log) {
            $em->remove($log);
        }
        $em->flush();
        $article = $em->getRepository('BazaWiedzyBundle:BWArtykul')->find($data['idArticle']);
        $em->remove($article);
        $em->flush();
        return new Response(
                '<html><body>Usunięto pomyślnie</body></html>'
        );
    }

    public function createBWLog($idArtykul) {
        $log = new BWHistoriaZmian();
        $em = $this->getDoctrine()->getManager();
        $log->setIdArtykul($em->getReference('BazaWiedzyBundle:BWArtykul', $idArtykul));
        $log->setIdUzytkownik($em->getReference('OcenyBundle:Uzytkownik', $this->getUser()->getId()));
        $log->setData((new \DateTime()));
        $em->persist($log);
        return true;
    }

}
