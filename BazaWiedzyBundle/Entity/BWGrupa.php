<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWGrupa
 *
 * @ORM\Table(name="b_w_grupa")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWGrupaRepository")
 */
class BWGrupa {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="BWGrupyCzlownkowie", mappedBy="idGrupa")
     * @ORM\OneToMany(targetEntity="BWDostep", mappedBy="idGrupa")

     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=255, unique=true)
     */
    private $nazwa;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nazwa.
     *
     * @param string $nazwa
     *
     * @return BWGrupa
     */
    public function setNazwa($nazwa) {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa.
     *
     * @return string
     */
    public function getNazwa() {
        return $this->nazwa;
    }

}
