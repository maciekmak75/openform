<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWDostep
 *
 * @ORM\Table(name="b_w_dostep")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWDostepRepository")
 */
class BWDostep {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWGrupa", inversedBy="id")

     */
    private $idGrupa;
    
        /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWKategorie", inversedBy="id")

     */
    private $idKategorie;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set idGrupa.
     *
     * @param \BazaWiedzyBundle\Entity\BWGrupa|null $idGrupa
     *
     * @return BWDostep
     */
    public function setIdGrupa(\BazaWiedzyBundle\Entity\BWGrupa $idGrupa = null)
    {
        $this->idGrupa = $idGrupa;

        return $this;
    }

    /**
     * Get idGrupa.
     *
     * @return \BazaWiedzyBundle\Entity\BWGrupa|null
     */
    public function getIdGrupa()
    {
        return $this->idGrupa;
    }

    /**
     * Set idKategorie.
     *
     * @param \BazaWiedzyBundle\Entity\BWKategorie|null $idKategorie
     *
     * @return BWDostep
     */
    public function setIdKategorie(\BazaWiedzyBundle\Entity\BWKategorie $idKategorie = null)
    {
        $this->idKategorie = $idKategorie;

        return $this;
    }

    /**
     * Get idKategorie.
     *
     * @return \BazaWiedzyBundle\Entity\BWKategorie|null
     */
    public function getIdKategorie()
    {
        return $this->idKategorie;
    }
}
