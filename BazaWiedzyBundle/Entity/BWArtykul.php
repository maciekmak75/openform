<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWArtykul
 *
 * @ORM\Table(name="b_w_artykul")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWArtykulRepository")
 */
class BWArtykul {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="BWPliki", mappedBy="idArtykul")
     * @ORM\OneToMany(targetEntity="BWHistoriaZmian", mappedBy="idArtykul")

     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="opis", type="string", length=1024, nullable=true)
     */
    private $opis;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tytul", type="string", length=255, nullable=true)
     */
    private $tytul;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWKategorie", inversedBy="id")

     */
    private $idKategorie;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set opis.
     *
     * @param string|null $opis
     *
     * @return BWArtykul
     */
    public function setOpis($opis = null) {
        $this->opis = $opis;

        return $this;
    }

    /**
     * Get opis.
     *
     * @return string|null
     */
    public function getOpis() {
        return $this->opis;
    }

    /**
     * Set idKategorie.
     *
     * @param \BazaWiedzyBundle\Entity\BWKategorie|null $idKategorie
     *
     * @return BWArtykul
     */
    public function setIdKategorie(\BazaWiedzyBundle\Entity\BWKategorie $idKategorie = null) {
        $this->idKategorie = $idKategorie;

        return $this;
    }

    /**
     * Get idKategorie.
     *
     * @return \BazaWiedzyBundle\Entity\BWKategorie|null
     */
    public function getIdKategorie() {
        return $this->idKategorie;
    }


    /**
     * Set tytul.
     *
     * @param string|null $tytul
     *
     * @return BWArtykul
     */
    public function setTytul($tytul = null)
    {
        $this->tytul = $tytul;

        return $this;
    }

    /**
     * Get tytul.
     *
     * @return string|null
     */
    public function getTytul()
    {
        return $this->tytul;
    }
}
