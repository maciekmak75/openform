<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWKategorie
 *
 * @ORM\Table(name="b_w_kategorie")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWKategorieRepository")
 */
class BWKategorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="BWDzial", mappedBy="idRodzic")
     * @ORM\OneToMany(targetEntity="BWArtykul", mappedBy="idKategorie")
     * @ORM\OneToMany(targetEntity="BWDostep", mappedBy="idKategorie")

     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nazwa", type="string", length=255)
     */
    private $nazwa;

        /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWKategorie", inversedBy="id")

     */
    private $idRodzic;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa.
     *
     * @param string $nazwa
     *
     * @return BWKategorie
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa.
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set idRodzic.
     *
     * @param \BazaWiedzyBundle\Entity\BWKategorie|null $idRodzic
     *
     * @return BWKategorie
     */
    public function setIdRodzic(\BazaWiedzyBundle\Entity\BWKategorie $idRodzic = null)
    {
        $this->idRodzic = $idRodzic;

        return $this;
    }

    /**
     * Get idRodzic.
     *
     * @return \BazaWiedzyBundle\Entity\BWKategorie|null
     */
    public function getIdRodzic()
    {
        return $this->idRodzic;
    }
}
