<?php

namespace BazaWiedzyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BWHistoriaZmian
 *
 * @ORM\Table(name="b_w_historia_zmian")
 * @ORM\Entity(repositoryClass="BazaWiedzyBundle\Repository\BWHistoriaZmianRepository")
 */
class BWHistoriaZmian {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="BWArtykul", inversedBy="id")

     */
    private $idArtykul;
    
        /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="OcenyBundle\Entity\Uzytkownik", inversedBy="id")

     */
    private $idUzytkownik;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set data.
     *
     * @param \DateTime $data
     *
     * @return BWHistoriaZmian
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return \DateTime
     */
    public function getData() {
        return $this->data;
    }


    /**
     * Set idArtykul.
     *
     * @param \BazaWiedzyBundle\Entity\BWArtykul|null $idArtykul
     *
     * @return BWHistoriaZmian
     */
    public function setIdArtykul(\BazaWiedzyBundle\Entity\BWArtykul $idArtykul = null)
    {
        $this->idArtykul = $idArtykul;

        return $this;
    }

    /**
     * Get idArtykul.
     *
     * @return \BazaWiedzyBundle\Entity\BWArtykul|null
     */
    public function getIdArtykul()
    {
        return $this->idArtykul;
    }

    /**
     * Set idUzytkownik.
     *
     * @param \OcenyBundle\Entity\Uzytkownik|null $idUzytkownik
     *
     * @return BWHistoriaZmian
     */
    public function setIdUzytkownik(\OcenyBundle\Entity\Uzytkownik $idUzytkownik = null)
    {
        $this->idUzytkownik = $idUzytkownik;

        return $this;
    }

    /**
     * Get idUzytkownik.
     *
     * @return \OcenyBundle\Entity\Uzytkownik|null
     */
    public function getIdUzytkownik()
    {
        return $this->idUzytkownik;
    }
}
