<?php

namespace BazaWiedzyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class BazaWiedzyUprawnieniaController extends Controller {

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bwUprawnienia", name="bwUprawnienia")
     */
    public function bwUprawnieniaAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('BazaWiedzyBundle:BWGrupa')->findAll();
        return $this->render('BazaWiedzyBundle::bwUprawnienia.html.twig', array('entities' => $entities));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_getUprawnieniaTree" ,name = "bw_getUprawnieniaTree", options={"expose"=true})
     */
    public function bw_getUprawnieniaTreeAction() {
        $em = $this->getDoctrine()->getManager();
        $tree = $em->getRepository('BazaWiedzyBundle:BWKategorie')->findAll();

        if (!$tree) {
            return $this->json([]);
        }
        foreach ($tree as $row) {
            $sub_data['key'] = $row->getId();
            $sub_data['folder'] = true; // $em->getRepository('BazaWiedzyBundle:BWKategorie')->findByIdRodzic($row->getId()) ? true : false;
            $sub_data['title'] = $row->getNazwa();
            $sub_data['parent_id'] = $row->getIdRodzic() ? $row->getIdRodzic()->getId() : false;
            $data[] = $sub_data;
        }
        foreach ($data as $key => &$value) {
            $output[$value['key']] = &$value;
        }
        foreach ($data as $key => &$value) {
            if ($value['parent_id'] && isset($output[$value['parent_id']])) {
                $output[$value['parent_id']]['children'][] = &$value;
            }
        }
        foreach ($data as $key => &$value) {
            if ($value["parent_id"] && isset($output[$value["parent_id"]])) {
                unset($data[$key]);
            }
        }
        /* echo '<pre>';
          \Doctrine\Common\Util\Debug::dump($data);
          echo '</pre>'; */
        return $this->json($data);

        //return $this->render('BazaWiedzyBundle::bazaWiedzy.html.twig');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/bw_updateUprawnieniaTree" ,name = "bw_updateUprawnieniaTree", options={"expose"=true})
     */
    public function bw_updateUprawnieniaTreeAction() {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        
        
    }

}
